package goutil_test

import (
	"testing"

	"gitee.com/ckbabby/goutil"
	"github.com/stretchr/testify/assert"
)

func TestFuncName(t *testing.T) {
	name := goutil.FuncName(goutil.PkgName)
	assert.Equal(t, "gitee.com/ckbabby/goutil.PkgName", name)
}

func TestPkgName(t *testing.T) {
	name := goutil.PkgName()
	assert.Equal(t, "goutil", name)
}
